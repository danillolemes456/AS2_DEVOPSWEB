import { render, screen } from '@testing-library/react';
import App from './App';

test('TESTE TELA LOGIN AS2', () => {
  render(<App />);

  expect(screen.getByPlaceholderText('Email'));
  expect(screen.getByPlaceholderText('Senha'));
  expect(screen.getByText("Entrar").closest('button')).not.toBeDisabled();
  expect(screen.getByText("Cadastro").closest('button')).not.toBeDisabled();

});
